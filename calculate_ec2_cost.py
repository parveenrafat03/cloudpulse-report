import json
import logging
from functools import lru_cache

import boto3

from cost_calculator import (availability_zone_to_region,
                             convert_operating_system)

logging.basicConfig(level=logging.INFO)


@lru_cache(maxsize=None)
def get_pricing_data(instance_type, operating_system, availability_zone, region):
    try:
        # This function should fetch pricing data for a given set of parameters
        # You can adapt it to use the AWS Price List Service or any other caching strategy
        human_readable_region, _ = availability_zone_to_region(availability_zone)
        pricing_client = boto3.client("pricing", region_name=region)
        response = pricing_client.get_products(
            ServiceCode="AmazonEC2",
            Filters=[
                {"Type": "TERM_MATCH", "Field": "instanceType", "Value": instance_type},
                {
                    "Type": "TERM_MATCH",
                    "Field": "operatingSystem",
                    "Value": operating_system,
                },
                {"Type": "TERM_MATCH", "Field": "tenancy", "Value": "Shared"},
                {"Type": "TERM_MATCH", "Field": "preInstalledSw", "Value": "NA"},
                {
                    "Type": "TERM_MATCH",
                    "Field": "location",
                    "Value": human_readable_region,
                },
                {"Type": "TERM_MATCH", "Field": "capacitystatus", "Value": "Used"},
                {"Type": "TERM_MATCH", "Field": "locationType", "Value": "AWS Region"},
                {"Type": "TERM_MATCH", "Field": "operation", "Value": "RunInstances"},
                {"Type": "TERM_MATCH", "Field": "marketoption", "Value": "OnDemand"},
            ],
        )

        if "PriceList" in response and response["PriceList"]:
            response_data = json.loads(response["PriceList"][0])
            on_demand_price = None
            for offer_key, offer_value in (
                response_data.get("terms", {}).get("OnDemand", {}).items()
            ):
                price_dimensions = offer_value.get("priceDimensions", {})
                for dimension_key, dimension_value in price_dimensions.items():
                    on_demand_price = dimension_value.get("pricePerUnit", {}).get("USD")

            return float(on_demand_price) if on_demand_price else None
        else:
            return None
    except Exception as e:
        logging.error("Error fetching On-Demand instance cost: %s", str(e))
        return None


def get_on_demand_instance_cost(instance_type, operating_system, availability_zone):
    try:
        human_readable_region, region = availability_zone_to_region(availability_zone)
        logging.info("Region value: %s", human_readable_region)

        # Use the caching function to fetch pricing data
        pricing_data = get_pricing_data(
            instance_type, operating_system, availability_zone, region
        )

        if pricing_data is not None:
            logging.info(f"DEBUG: on_demand_price: {pricing_data}")
            return pricing_data
        else:
            return None
    except Exception as e:
        logging.error("Error fetching On-Demand instance cost: %s", str(e))
        return None


def get_ec2_instance_cost_off_hours(resource_data):
    prices = {}
    for instance_data in resource_data:
        instance_type = instance_data.get("InstanceType")
        operating_system = convert_operating_system(
            instance_data.get("PlatformDetails")
        )
        placement = instance_data.get("Placement", {})
        availability_zone = placement.get("AvailabilityZone", "")

        on_demand_price = get_on_demand_instance_cost(
            instance_type, operating_system, availability_zone
        )

        if on_demand_price is not None:
            # Update the prices dictionary
            prices.setdefault(instance_type, []).append(on_demand_price)

    # Calculate the total cost for running instances
    total_cost = 0
    for instance_type, prices_list in prices.items():
        count = len(prices_list)  # Count of instances for the current type
        cost_per_hour = sum(prices_list) / count if count > 0 else 0
        total_cost += cost_per_hour * count * 12 * 30  # Fixed duration of 12 hours

    return total_cost


# def get_ec2_instance_cost_generic(resource_data):
#     prices = {}
#     for instance_data in resource_data:
#         instance_type = instance_data.get("InstanceType")
#         operating_system = convert_operating_system(
#             instance_data.get("PlatformDetails")
#         )
#         placement = instance_data.get("Placement", {})
#         availability_zone = placement.get("AvailabilityZone", "")

#         on_demand_price = get_on_demand_instance_cost(
#             instance_type, operating_system, availability_zone
#         )

#         if on_demand_price is not None:
#             # Update the prices dictionary
#             prices.setdefault(instance_type, []).append(on_demand_price)

#     # Calculate the total cost for running instances
#     total_cost = 0
#     for instance_type, prices_list in prices.items():
#         count = len(prices_list)  # Count of instances for the current type
#         cost_per_hour = sum(prices_list) / count if count > 0 else 0
#         total_cost += cost_per_hour * count * 24 * 30  # Fixed duration of 12 hours

#     return total_cost


def get_ec2_instance_cost_recommended(resource_data):
    prices = {}
    for instance_data in resource_data:
        instance_type = instance_data.get("InstanceType")
        operating_system = convert_operating_system(
            instance_data.get("PlatformDetails")
        )
        placement = instance_data.get("Placement", {})
        availability_zone = placement.get("AvailabilityZone", "")

        on_demand_price = get_on_demand_instance_cost(
            instance_type, operating_system, availability_zone
        )

        if on_demand_price is not None:
            # Update the prices dictionary
            prices.setdefault(instance_type, []).append(on_demand_price)

    # Calculate the total cost for running instances
    total_cost = 0
    for instance_type, prices_list in prices.items():
        count = len(prices_list)  # Count of instances for the current type
        cost_per_hour = sum(prices_list) / count if count > 0 else 0
        total_cost += cost_per_hour * count * 24 * 30  # Fixed duration of 24 hours

    return total_cost


def get_ec2_instance_cost_recommended_from_rds(
    recommended_instance_type, resource_data
):
    prices = {}
    for instance_data in resource_data:
        operating_system = convert_operating_system(
            instance_data.get("PlatformDetails")
        )
        placement = instance_data.get("Placement", {})
        availability_zone = placement.get("AvailabilityZone", "")

        on_demand_price = get_on_demand_instance_cost(
            recommended_instance_type, operating_system, availability_zone
        )

        if on_demand_price is not None:
            # Update the prices dictionary
            prices.setdefault(recommended_instance_type, []).append(on_demand_price)

    # Calculate the total cost for running instances
    total_cost = 0
    for recommended_instance_type, prices_list in prices.items():
        count = len(prices_list)  # Count of instances for the current type
        cost_per_hour = sum(prices_list) / count if count > 0 else 0
        total_cost += cost_per_hour * count * 24 * 30  # Fixed duration of 24 hours

    return total_cost
